// .bounce, .flash, .pulse, .shake, .swing, .tada, .wobble, .bounceIn, .bounceInDown, .bounceInLeft, .bounceInRight, .bounceInUp, .bounceOut, .bounceOutDown, .bounceOutLeft, .bounceOutRight, .bounceOutUp, .fadeIn, .fadeInDown, .fadeInDownBig, .fadeInLeft, .fadeInLeftBig, .fadeInRight, .fadeInRightBig, .fadeInUp, .fadeInUpBig, .fadeOut, .fadeOutDown, .fadeOutDownBig, .fadeOutLeft, .fadeOutLeftBig, .fadeOutRight, .fadeOutRightBig, .fadeOutUp, .fadeOutUpBig, .flip, .flipInX, .flipInY, .flipOutX, .flipOutY, .lightSpeedIn, .lightSpeedOut, .rotateIn, .rotateInDownLeft, .rotateInDownRight, .rotateInUpLeft, .rotateInUpRight, .rotateOut, .rotateOutDownLeft, .rotateOutDownRight, .rotateOutUpLeft, .rotateOutUpRight, .slideInDown, .slideInLeft, .slideInRight, .slideOutLeft, .slideOutRight, .slideOutUp, .rollIn, .rollOut, .zoomIn, .zoomInDown, .zoomInLeft, .zoomInRight, .zoomInUp
jQuery.noConflict();
jQuery(window).load(function() {
    jQuery(window).scroll(function() {
        // This is then function used to detect if the element is scrolled into view
        function elementScrolled(elem) {
            var docViewTop = jQuery(window).scrollTop();
            var docViewBottom = docViewTop + jQuery(window).height();
            var elemTop = jQuery(elem).offset().top;
            return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
        }

        // This is where we use the function to detect if ".box2" is scrolled into view, and when it is add the class ".animated" to the <p> child element
        if (elementScrolled('.container .threebox')) {
            var els = jQuery('.container .threebox'),
                i = 0,
                f = function() {
                    jQuery(els[i++]).addClass('fadeInDown');
                    if (i < els.length) setTimeout(f, 400);
                };
            f();
        }

        if (elementScrolled('#section2 .container')) {
            var els = jQuery('#section2 .container'),
                i = 0,
                f = function() {
                    jQuery(els[i++]).addClass('fadeInRight');
                    if (i < els.length) setTimeout(f, 400);
                };
            f();
        }

        if (elementScrolled('#section3 .container')) {
            var els = jQuery('#section3 .container'),
                i = 0,
                f = function() {
                    jQuery(els[i++]).addClass('fadeInUp');
                    if (i < els.length) setTimeout(f, 400);
                };
            f();
        }

        // if(elementScrolled('#section4 .container')) {
        //     var els = jQuery('#section4 .container'),
        //         i = 0,
        //         f = function () {
        //             jQuery(els[i++]).addClass('fadeInLeft');
        //             if(i < els.length) setTimeout(f, 400);
        //         };
        //     f();
        // }

        // if(elementScrolled('#section5 .container')) {
        //     var els = jQuery('#section5 .container'),
        //         i = 0,
        //         f = function () {
        //             jQuery(els[i++]).addClass('fadeInUp');
        //             if(i < els.length) setTimeout(f, 400);
        //         };
        //     f();
        // }

        if (elementScrolled('#section6 .container')) {
            var els = jQuery('#section6 .container'),
                i = 0,
                f = function() {
                    jQuery(els[i++]).addClass('fadeInRight');
                    if (i < els.length) setTimeout(f, 400);
                };
            f();
        }

        if (elementScrolled('#section7 .container')) {
            var els = jQuery('#section7 .container'),
                i = 0,
                f = function() {
                    jQuery(els[i++]).addClass('fadeInUp');
                    if (i < els.length) setTimeout(f, 400);
                };
            f();
        }

        if (elementScrolled('#section8 .news-box')) {
            var els = jQuery('#section8 .news-box'),
                i = 0,
                f = function() {
                    jQuery(els[i++]).addClass('fadeInRight');
                    if (i < els.length) setTimeout(f, 400);
                };
            f();
        }
        if (elementScrolled('#section9 .container')) {
            var els = jQuery('#section9 .container'),
                i = 0,
                f = function() {
                    jQuery(els[i++]).addClass('fadeInUp');
                    if (i < els.length) setTimeout(f, 400);
                };
            f();
        }

        if (elementScrolled('#section10 .container')) {
            var els = jQuery('#section10 .container'),
                i = 0,
                f = function() {
                    jQuery(els[i++]).addClass('fadeInUp');
                    if (i < els.length) setTimeout(f, 400);
                };
            f();
        }
        if (elementScrolled('#footer-wrapper .container')) {
            var els = jQuery('#footer-wrapper .container'),
                i = 0,
                f = function() {
                    jQuery(els[i++]).addClass('fadeInUp');
                    if (i < els.length) setTimeout(f, 400);
                };
            f();
        }

    });
});